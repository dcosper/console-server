#include "BaseServer.h"

BaseServer::BaseServer()
{
}

BaseServer::~BaseServer()
{
}
// Globals
// Port
int _port = 7002;
// Accept callers or not
static bool _isRunning = false;

static const int opt = TRUE;

static int master_socket,
       addrlen,
       new_socket,
       activity,
       valread,
       sd,
       max_sd;

static struct sockaddr_in address;

// All connected clients
static std::vector<TCPClient*> ConnectedClients;

bool BaseServer::isRunning()
{
    return _isRunning;
}
// load port n shit from config
void BaseServer::SetServerState(bool state)
{
    if(state)
    {
        printf("Accepting connections.\n");
    }
    else printf("No longer accepting connections.\n");
    _isRunning = state;
}
void BaseServer::SetPort(int port)
{
    _port = port;
}
int BaseServer::GetUserCount()
{
    return (int)ConnectedClients.size();
}
int BaseServer::GetAllConnectedSockets()
{
    return 0;
}
void BaseServer::DisconnectAllSockets()
{
    for(uint32_t i =0;i<ConnectedClients.size();i++)
    {
        ConnectedClients.at(i)->Close();
    }
}

bool BaseServer::DisconnectSocketAt(int position)
{
    if(position > (int)ConnectedClients.size())return false;

    ConnectedClients.at(position)->Close();

    return true;
}
TCPClient* BaseServer::GetClient(int sockFD)
{
    for(uint32_t i=0; i<ConnectedClients.size(); i++)
    {
        if(ConnectedClients[i]->SocketFD == sockFD)
            return ConnectedClients.at(i);
    }
    return NULL;
}
std::vector<char> SayHello()
{
    PacketWriter *writer = new PacketWriter;

    // Write opCode
    writer->WriteShort(_IPC_SERVER_HELLO);
    std::vector<char> result = writer->GetFinalPacket();
   // free(writer);
    return result;
}
void BaseServer::ProcessMessage(TCPClient *client, char* message, int length)
{
}

void* BaseServer::Start(void *arg)
{
    BaseServer* server = (BaseServer*)arg;

    if(!server)exit(0);

    char buffer[1025]; //data buffer of 1K

    //set of socket descriptors
    fd_set readfds;

    //a message
    std::vector<char> message = SayHello();//"TCPServer Base v1.0 \r\n";

    //create a master socket
    if( (master_socket = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    //set master socket to allow multiple connections ,
    //this is just a good habit, it will work without this
    if( setsockopt(master_socket, SOL_SOCKET, SO_REUSEADDR, (char *)&opt,
                   sizeof(opt)) < 0 )
    {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }
    //type of socket created
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(  _port );

    //bind the socket to localhost port 8888
    if (bind(master_socket, (struct sockaddr *)&address, sizeof(address))<0)
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }
    printf("Listening on port %d.\n", _port );

    //try to specify maximum of 3 pending connections for the master socket
    if (listen(master_socket, 3) < 0)
    {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    //accept the incoming connection
    addrlen = sizeof(address);
    puts("Waiting for connections ...\r\n");

    while( isRunning() )
    {
        //clear the socket set
        FD_ZERO(&readfds);

        //add master socket to set
        FD_SET(master_socket, &readfds);
        max_sd = master_socket;

            for(uint32_t i =0;i<ConnectedClients.size();i++)
            {
                sd = ConnectedClients.at(i)->SocketFD;

                if( sd > 0 )
                    FD_SET( sd, &readfds );

                if(sd > max_sd )
                    max_sd = sd;
            }

        //wait for an activity on one of the sockets , timeout is NULL ,
        //so wait indefinitely
        activity = select( max_sd + 1, &readfds, NULL, NULL, NULL);

        if ((activity < 0) && (errno!=EINTR))
        {
            printf("select error");
        }

        //If something happened on the master socket ,
        //then its an incoming connection
        if (FD_ISSET(master_socket, &readfds))
        {
            if ((new_socket = accept(master_socket,
                                     (struct sockaddr *)&address, (socklen_t*)&addrlen))<0)
            {
                perror("accept");
                exit(EXIT_FAILURE);
            }

            // Make our new client
            TCPClient *client = new TCPClient(new_socket, address);

            printf("My New Connection, SocketFD %d IP: %s Port: %d.\n", client->SocketFD, client->RemoteEndPoint().c_str(), client->RemotePort());


            //send new connection greeting message
            // First time we speak to client
            // my send
            if(!client->Send(message))
            {
                printf("Send message to socket %d failed!", client->SocketFD);
                perror("Sending error");
            }

            puts("Welcome message sent successfully");

            ConnectedClients.push_back(client);
        }

        for(uint32_t i=0; i<ConnectedClients.size(); i++)
        {
            sd = ConnectedClients.at(i)->SocketFD;

            if(FD_ISSET( sd, &readfds))
            {
                if ((valread = read( sd, buffer, 1024)) == 0)
                {
                    printf("TcpClient %s port %d disconnected.\n", ConnectedClients.at(i)->RemoteEndPoint().c_str(), ConnectedClients.at(i)->RemotePort());
                    //Close the socket and mark as 0 in list for reuse
                    ConnectedClients.at(i)->Close();
                    // remove client from vector
                    ConnectedClients.erase(ConnectedClients.begin() + i);
                }

                //Echo back the message that came in
                else
                {
                    // Handle data
                    //set the string terminating NULL byte on the end
                    //of the data read
                    buffer[valread] = '\0';

                    // Dont process if we are empty
                    if(buffer[0] != '\r' && buffer[0] != '\n')
                    server->ProcessMessage(ConnectedClients.at(i) , buffer, valread);
                }
            }
        }
    }
    return NULL;
}
void BaseServer::StartServer()
{
   // SetServerState(true);
   // Start((void*)1);

    int error;
    pthread_t threadID[1];

    error = pthread_create(&(threadID[0]), NULL, &Start, this);

    if(error !=0)
    {
        printf("Unable to start server in it's own thread aborting...");
        exit(0);
    }
    else SetServerState(true);

}
