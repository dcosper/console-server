#ifndef BASESERVER_H
#define BASESERVER_H

#include "system.h"


#define TRUE 1
#define FALSE 0

#define _MAX_CLIENTS 3000
class TCPClient;

class BaseServer
{
    public:
        BaseServer();
        virtual ~BaseServer();

        void StartServer();

        void SetPort(int port);

        void SetServerState(bool state);

        int GetUserCount();

        void DumpAllUsers();

        int GetAllConnectedSockets();

        void DisconnectAllSockets();

        bool DisconnectSocketAt(int position);


        virtual void ProcessMessage(TCPClient *client, char* message, int length);

        static void* Start(void *arg);

        static bool isRunning();



        TCPClient* GetClient(int socketFD);

    protected:
    private:


};

#endif // BASESERVER_H
