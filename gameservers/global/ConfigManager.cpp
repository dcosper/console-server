/*  Simple Config file reader
    Reads just a few vars in using name=value
    Daniel Cosper 4-12-2017
*/
#include "ConfigManager.h"

ConfigManager::ConfigManager()
{
    //ctor
}

ConfigManager::~ConfigManager()
{
    //dtor
}
std::string get_right_of_delim(std::string const& str, std::string const& delim){
  return str.substr(str.find(delim) + delim.size());
}
bool ConfigManager::LoadConfig(const std::string &fileName)
{
    std::string line;
    std::ifstream confFile (fileName);

    if(confFile.is_open())
    {
        while( getline (confFile, line))
        {
            if( strncmp( line.c_str(), "MySQLHost", strlen("MySQLHost")) == 0)
                MySQLHost = get_right_of_delim(line, "=");

            if( strncmp( line.c_str(), "MySQLUser", strlen("MySQLUser")) == 0)
                MySQLUser = get_right_of_delim(line, "=");

            if( strncmp( line.c_str(), "MySQLPass", strlen("MySQLPass")) == 0)
                MySQLPass = get_right_of_delim(line, "=");

            if( strncmp( line.c_str(), "MySQLDatabase", strlen("MySQLDatabase")) == 0)
                MySQLDatabase = get_right_of_delim(line, "=");

            if( strncmp( line.c_str(), "ServerPort", strlen("ServerPort")) == 0)
                ServerPort = get_right_of_delim(line, "=");
        }
        confFile.close();
        // Check to see if all our varibles populated
        return true;
    }
    else printf("Unable to open file %s!\n", fileName.c_str());
    return false;
}
