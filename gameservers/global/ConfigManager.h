/*  Simple Config file reader
    Reads just a few vars in using name=value
    Daniel Cosper 4-12-2017
*/
#ifndef CONFIGMANAGER_H
#define CONFIGMANAGER_H

#include "system.h"

class ConfigManager
{
    public:
        ConfigManager();
        virtual ~ConfigManager();

        bool LoadConfig(const std::string &fileName);

    std::string MySQLHost;
    std::string MySQLUser;
    std::string MySQLPass;
    std::string MySQLDatabase;
    std::string ServerPort;

    protected:

    private:

};

#endif // CONFIGMANAGER_H
