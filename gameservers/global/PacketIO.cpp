#include "PacketIO.h"

// data unions
union int8_char {
    int8_t byteValue = 0;
    char charValue[1];
};
union int16_char{
    int16_t byteValue = 0;
    char charValue[2];
};
union int32_char{
    int32_t byteValue = 0;
    char charValue[4];
};
union int64_char{
    int64_t byteValue = 0;
    char charValue[8];
};
union uint8_char {
    uint8_t byteValue = 0;
    char charValue[1];
};
union uint16_char{
    uint16_t byteValue = 0;
    char charValue[2];
};
union uint32_char{
    uint32_t byteValue = 0;
    char charValue[4];
};
union uint64_char{
    uint64_t byteValue = 0;
    char charValue[8];
};
///

PacketReader::PacketReader(char* packet, int length)
{
    // Load our data into our vector
    for(int i=0;i<length;i++)
    {
        data.push_back(packet[i]);
    }
}
PacketReader::~PacketReader()
{
    //dtor
    //delete[] tmp;
}

// Reads first char from stream and advances stream by 1
char PacketReader::ReadByte()
{
    if(data.size() < 1)return -1;
    char result = data.at(0);
    data.erase(data.begin());
    return result;
}
char* PacketReader::ReadBytes(int32_t number)
{
    if((int32_t)data.size() < number)return NULL;

    char* tmp = new char[number];

    for(int32_t i =0;i<number;i++)
    {
        tmp[i] = data.at(0);
        data.erase(data.begin());
    }
    return tmp;
}
uint16_t PacketReader::ReadUShort()
{
    if(data.size() < sizeof(uint16_t))return 0;

    uint16_char val;
    for(uint16_t i=0;i<sizeof(uint16_t);i++)
    {
        val.charValue[i] = data.at(0);
        data.erase(data.begin());
    }
    return val.byteValue;
}
uint32_t PacketReader::ReadUInt()
{
    if(data.size() < sizeof(uint32_t))return 0;

    uint32_char val;
    for(uint16_t i=0;i<sizeof(uint32_t);i++)
    {
        val.charValue[i] = data.at(0);
        data.erase(data.begin());
    }
    return val.byteValue;
}
uint64_t PacketReader::ReadULong()
{
    if(data.size() < sizeof(uint64_t))return 0;

    uint64_char val;
    for(uint16_t i=0;i<sizeof(uint64_t);i++)
    {
        val.charValue[i] = data.at(0);
        data.erase(data.begin());
    }
    return val.byteValue;
}
int16_t PacketReader::ReadShort()
{
    if(data.size() < sizeof(int16_t)) return -1;
    int16_char val;

    for(uint i=0;i<sizeof(int16_t);i++)
    {
        val.charValue[i] = data.at(0);
        data.erase(data.begin());
    }
    return val.byteValue;
}
int32_t PacketReader::ReadInt()
{
    if(data.size()< sizeof(int32_t)) return -1;
    int32_char val;

    for(uint16_t i=0;i<sizeof(int32_t);i++)
    {
        val.charValue[i] = data.at(0);
        data.erase(data.begin());
    }
    return val.byteValue;
}
int64_t PacketReader::ReadLong()
{
    if(data.size() < sizeof(int64_t))return 0;

    int64_char val;
    for(uint16_t i=0;i<sizeof(uint64_t);i++)
    {
        val.charValue[i] = data.at(0);
        data.erase(data.begin());
    }
    return val.byteValue;
}
std::string PacketReader::ReadString()
{
    std::string result;
    //first get length
    uint16_t len = ReadShort();

    if(len <=0) // no length to our string ?
    return "";

    if(data.size() < len) // we dont have enough data for this string
        return "";

    char* c_result = new char[len];

    for(int i=0;i<len;i++)
    {
        c_result[i] = data.at(0);
        data.erase(data.begin());
    }
    return result.assign(c_result, len);
}
char* PacketReader::EncryptData(char* unEncryptedData)
{
    // not done yet
    return NULL;
}
char* PacketReader::UnEncryptData(char* encryptedData)
{
    // not done yet
    return NULL;
}

//______________________________Packet Writer ___________________________
PacketWriter::PacketWriter()
{
    //ctor
//    delete[] data;
    packet.clear();
}

PacketWriter::~PacketWriter()
{
    //dtor
   // delete[] data;
    packet.clear();
}
const char PacketWriter::WriteByte(int8_t value)
{
    int8_char byte;
    byte.byteValue = value;

    packet.push_back(byte.charValue[0]);
    WritePosition++;
    return byte.charValue[0];
}
const char* PacketWriter::WriteBytes(const char* value, int length)
{
    for(int i=0;i<length;i++)
    {
        packet.push_back(value[i]);
        WritePosition++;
    }
    return value;
}
uint16_t PacketWriter::WriteUShort(uint16_t value)
{
    uint16_char val;
    val.byteValue=value;

    for(uint16_t i=0;i<sizeof(uint16_t);i++)
        packet.push_back(val.charValue[i]);

    return val.byteValue;
}
uint32_t PacketWriter::WriteUInt(uint32_t value)
{
    uint32_char val;
    val.byteValue = value;

    for(uint16_t i =0;i<sizeof(uint32_t);i++)
    {
        packet.push_back(val.charValue[i]);
        WritePosition++;
    }
    return val.byteValue;
}
uint64_t PacketWriter::WriteULong(uint64_t value)
{
    uint64_char val;
    val.byteValue = value;

    for(uint16_t i=0;i<sizeof(uint64_t);i++)
    {
        packet.push_back(val.charValue[i]);
        WritePosition++;
    }
    return value;
}
int16_t PacketWriter::WriteShort(int16_t value)
{
    // new way
    int16_char svalue;
    svalue.byteValue = value;

    for(uint i=0;i<sizeof(int16_t);i++)
        packet.push_back(svalue.charValue[i]);

    return svalue.byteValue;
}
int32_t PacketWriter::WriteInt(int32_t value)
{
    int32_char val;
    val.byteValue = value;

    for(uint16_t i =0;i<sizeof(int32_t);i++)
    {
        packet.push_back(val.charValue[i]);
        WritePosition++;
    }
    return val.byteValue;
}
int64_t PacketWriter::WriteLong(int64_t value)
{
    int64_char val;
    val.byteValue = value;

    for(uint i=0;i<sizeof(int64_t);i++)
    {
        packet.push_back(val.charValue[i]);
        WritePosition++;
    }
    return val.byteValue;
}
int32_t PacketWriter::WriteString(std::string value)
{
    // write the string length
    WriteShort(value.size());
    // write the string
    for(uint16_t i=0;i<value.size();i++)
    {
        packet.push_back(value.c_str()[i]);
        WritePosition++;
    }
    return value.size();
}
std::vector<char> PacketWriter::GetFinalPacket()
{
    // we need to preface this with a short
    // that contains the length of the packet
    std::vector<char> finalPacket;


    // get the size of the payload
    int32_t length = (int32_t)packet.size();

    int32_char payLoadLength;
    payLoadLength.byteValue = length;

    for(uint32_t i=0;i<sizeof(int32_t);i++)
    {
        finalPacket.push_back(payLoadLength.charValue[i]);
    }
    // the payload which includes our opcode

    for(uint i=0;i<packet.size();i++)
    {
        finalPacket.push_back(packet.at(i));
    }

    // add our crc which is really just a int32
    int32_char crc;
    crc.byteValue = 43332; // 0 for now

    for(uint16_t i=0;i<sizeof(int32_t);i++)
        finalPacket.push_back(crc.charValue[i]);

    return finalPacket;
}
int PacketWriter::GetLength()
{
    return (int)packet.size();
}
int PacketWriter::GetWritePosition()
{
    return WritePosition;
}
