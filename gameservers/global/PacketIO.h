#ifndef PACKETIO_H
#define PACKETIO_H

#include "system.h"

class PacketReader
{
    public:
        PacketReader(char* packet, int length);
        virtual ~PacketReader();

        char* EncryptData(char* unEncryptedData);

        char* UnEncryptData(char* encryptedData);

        char ReadByte();
        char* ReadBytes(int32_t number);
        uint16_t ReadUShort();
        uint32_t ReadUInt();
        uint64_t ReadULong();
        int16_t ReadShort();
        int32_t ReadInt();
        int64_t ReadLong();
        std::string ReadString();
    protected:
    std::vector<char> data;
    //char* tmp;
    int Position;
    private:

};

class PacketWriter
{
    public:
        PacketWriter();
        virtual ~PacketWriter();

        char* EncryptData(char* unEncryptedData);

        char* UnEncryptData(char* encryptedData);

        const char WriteByte(int8_t value);
        const char* WriteBytes(const char* value, int length);
        uint16_t WriteUShort(uint16_t value);
        uint32_t WriteUInt(uint32_t value);
        uint64_t WriteULong(uint64_t value);
        int16_t WriteShort(int16_t value);
        int32_t WriteInt(int32_t value);
        int64_t WriteLong(int64_t value);
        int32_t WriteString(std::string value);

        std::vector<char> GetFinalPacket();
        int GetLength();
        int GetWritePosition();

    protected:
     std::vector<char> packet;
     int WritePosition;
     int Length;
    private:

};
#endif // PACKETIO_H
