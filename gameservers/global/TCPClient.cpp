#include "TCPClient.h"

TCPClient::TCPClient(int sockFD, sockaddr_in address)
{
    SocketFD = sockFD;
    Address = address;
}

TCPClient::~TCPClient()
{
}

std::string TCPClient::RemoteEndPoint()
{
    return inet_ntoa(Address.sin_addr);
}

uint16_t TCPClient::RemotePort()
{
    return ntohs(Address.sin_port);
}
bool TCPClient::Send(std::vector<char> message)
{
    bool result = true;
    char data[message.size()];

    std::copy(message.begin(), message.end(), data);

    if( send( SocketFD, &data, (int)message.size(), 0) != (int)message.size() )
    {
        result = false;
    }
    return result;
}
void TCPClient::Close()
{
    close(SocketFD);
    SocketFD = 0;

    free(this);
}
