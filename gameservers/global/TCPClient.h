#ifndef TCPCLIENT_H
#define TCPCLIENT_H

#include "system.h"

class TCPClient
{
    public:
        TCPClient(int sockId, sockaddr_in address);
        virtual ~TCPClient();
    int SocketFD;
    sockaddr_in Address;

    std::string RemoteEndPoint();
    uint16_t RemotePort();
    // Send and shit
   // bool Send(std::vector<int> message);
    bool Send(std::vector<char> message);

    void Close();

    protected:

    private:
};

#endif // TCPCLIENT_H
