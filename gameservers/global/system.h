#ifndef SYSTEM_H
#define SYSTEM_H

#include <vector>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
#include <sstream>
#include <stdio.h>
#include <string.h> //strlen
#include <stdlib.h>
#include <cstdlib>
#include <errno.h>
#include <unistd.h> //close
#include <pthread.h> // for threading
#include <arpa/inet.h> //close
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/time.h> //FD_SET, FD_ISSET, FD_ZERO macros
#include <mysql.h>
#include "BaseServer.h"
#include "ConfigManager.h"
#include "Sql.h"
#include "TCPClient.h"
#include "TCPMessages.h"
#include "PacketIO.h"
#endif // SYSTEM_H
