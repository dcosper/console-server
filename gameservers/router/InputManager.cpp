//#include <memory>
#include "InputManager.h"

#if !defined(ARRAY_SIZE)
#define ARRAY_SIZE(x) (sizeof((x)) / sizeof((x)[0]))
#endif

InputManager::InputManager()
{
    //ctor
}

InputManager::~InputManager()
{
    //dtor
}
// The config reader
class ConfigReader : public ConfigManager
{

};

PacketReader *preader;
PacketWriter *pwriter;

// The server
class RoutingServer : public BaseServer
{

public:
    void ProcessMessage(TCPClient *client, char* message, int length);
};
// Process Messages for a client
void RoutingServer::ProcessMessage(TCPClient *client, char* message, int length)
{
    if(length < 0)return; // no data

    preader = new PacketReader(message, length);
    pwriter = new PacketWriter;

    // break the packet down
    int32_t payloadLen = preader->ReadInt();

    printf("We got payloadlen of %d.\n", length);

    if(payloadLen == -1)return;


    int16_t opCode = preader->ReadShort();
    if(opCode < 0)return;

    printf("We got opcode %d\n", opCode);

    char* payload;

    if(payloadLen - 2 > 0)
    {
        //payload = new char[payloadLen - 2];
        payload = preader->ReadBytes(payloadLen - 2);
        printf("We got the rest of the bytes %d.\n", payloadLen - 2);
    }


    int32_t crc = preader->ReadInt();
    printf("We read the crc of %d.\n", crc);
    if(crc < 0)return;

    switch(opCode)
    {
    case _IPC_CLIENT_HELLO:
    {
        // Client is replying to our hello
        // So now tell him we want him to authenicate
        pwriter->WriteShort(_IPC_SERVER_REQUEST_LOGON);

        std::vector<char> pfinal = pwriter->GetFinalPacket();

        // Send it
        client->Send(pfinal);
    }
    break;
    case _IPC_CLIENT_LOGON:
    {
        // Lets get user and pass from payload

        preader = new PacketReader(payload, payloadLen);

        std::string user = preader->ReadString();
        std::string pass = preader->ReadString();

        printf("We got username of %s and pass of %s.\n", user.c_str(), pass.c_str());
    }
    break;
    case _IPC_CLIENT_LOGOFF:
    {

    }
    break;
    case _IPC_TERMINATED:
        {
            printf("We lost a client.\n");
        }
        break;
    default:
    {
        printf("ERR: Client %s (SocketFD %d) attempted to send unknown opCode %d!!!\n", client->RemoteEndPoint().c_str(), client->SocketFD,opCode);
    }
    break;
    }
    //clean up
//    delete[] crc;
    //  delete[] payload;
    free(preader);
    free(pwriter);
}

void InputManager:: WaitForInput(void)
{
    isAccepting = true;

    while(isAccepting) // locks main thread
    {
        std::string token = " ";

        std::string line;

        Print(">>");

        std::getline(std::cin, line);

        std::stringstream stream( line );

        std::vector<std::string> tokens;

        size_t pos = std::string::npos; // size_t to avoid improbable overflow

        do
        {
            pos = line.find(token);
            tokens.push_back(line.substr(0, pos));
            if (std::string::npos != pos)
                line = line.substr(pos + token.size());
        }
        while (std::string::npos != pos);


        ProcessCommands((tokens));
    }
}
void InputManager::ProcessCommands(std::vector<std::string> args)
{
    // The server
    RoutingServer *server = new RoutingServer;
    ConfigReader *reader = new ConfigReader;

    if(args[0].empty())
    {
        return;
    }

    // Convert everything to lowercase
    // we do not need to have commands with uppercase chars ever
    for(uint i= 0; i<args.size(); i++)
    {
        std::transform( args[i].begin(), args[i].end(), args[i].begin(), ::tolower);
    }

    // Check for command and complete action

    if(args[0] == "start")
    {
        if(server)
        {
            server->SetPort(7010);
            server->StartServer();
        }
    }
    else if(args[0] == "close")
    {
        if(server)
        {
            if(server->isRunning())
                server->SetServerState(false);
            else PrintLine("Server already not accepting connections.");
        }
    }
    else if(args[0] == "open")
    {
        if(server)
        {
            if(!server->isRunning())
                server->SetServerState(true);
            else PrintLine("Server is already accepting connections.");
        }
    }
    else if (args[0] == "count")
    {
        if(server)
            printf("Currently %d accounts logged on.\n", server->GetUserCount());
    }
    else if (args[0] == "read")
    {
        if(reader)
        {
            if( reader->LoadConfig( _CONFIG ) )
            {
                // our varibles are populated
                printf("MySqlHost is (%s).\n", reader->MySQLHost.c_str());
                printf("MySqlUser is (%s).\n", reader->MySQLUser.c_str());
                printf("MySqlPass is (%s).\n", reader->MySQLPass.c_str());
                printf("MySqlDatabase is (%s).\n", reader->MySQLDatabase.c_str());
                printf("ServerPort is (%s).\n", reader->ServerPort.c_str());
            }
            else printf( "Unable to load config file %s!\n", _CONFIG );
            delete reader;
        }
    }
    else if (args[0] == "get")
    {
        // gets client by sockFD id
        if(args.size() != 2)
        {
            printf("Command Usage: get #id where #id is the socket number.\n");
            return;
        }
        int fd = atoi ( args[1].c_str());

        TCPClient *found = server->GetClient(fd);

        if(found)
        {
            printf("We fount a client\n");
        }
        else
        {
            printf("Unable to find a TCPClient with SocFD of %d!\n", fd);
            free (found );

        }

    }
    else if (args[0] == "test")
    {
        int16_t value = 500; // bytes 244 1 or 0xF4 0x01

        PacketWriter *w = new PacketWriter;
        w->WriteShort(value);

        std::vector<char> finalp = w->GetFinalPacket();

        printf("Packet should be 4 in length its %d.\n", finalp.size());

        for(uint16_t i=0; i<finalp.size(); i++)
            printf("C(%c) D(%d)\n", finalp.at(i), finalp.at(i));

    }
    else if (args[0] == "clear")
    {
        std::system("clear");
    }
    else if(args[0] == "exit")
    {
        // exit this console
        Exit(_EXIT_REASON_NORMAL);
    }
    else printf("Unknown Command %s!\n", args[0].c_str());

}
void InputManager::Print(std::string line)
{
    if(!line.empty())
        printf("%s", line.c_str());
}
void InputManager::PrintLine(std::string line)
{
    Print(line + "\n");
}
void InputManager::Exit(int exitReason)
{

    // Exit or crash details here

    switch(exitReason)
    {

    case 0:
    {
        PrintLine("Exiting Reason: Requested.");
        exit(0);
    }
    break;
    }
}

