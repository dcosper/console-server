#ifndef INPUTMANAGER_H
#define INPUTMANAGER_H

#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm>
#include <iomanip>
#include <cstdlib>
#include "../global/system.h"

#define _EXIT_REASON_NORMAL 0
#define _CONFIG "router.conf"

class InputManager
{
public:
    InputManager();
    virtual ~InputManager();

    void WaitForInput();
    void ProcessCommands(std::vector<std::string> args);
    void PrintLine(std::string line);
    void Print(std::string line);
    void Exit(int exitReason);
protected:

private:
    bool isAccepting;

};
#endif // INPUTMANAGER_H
