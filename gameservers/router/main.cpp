#include "InputManager.h"

using namespace std;
// The console boot message
std::string MOTD ="Command Console";

int main()
{
    InputManager *manager = new InputManager;

    manager->PrintLine(MOTD);

    manager->WaitForInput();
    return 0;
}
